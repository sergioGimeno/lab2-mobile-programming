package com.example.sergiogimeno.lab21;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class A3 extends AppCompatActivity {
    Spinner spinner;
    String links[];
    String linkSelected;
    ArrayAdapter<CharSequence> adapter;
    Button buttonA1;
    Button buttonA2;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3);
        links = getResources().getStringArray(R.array.FeedListLinks);
        buttonA1 = findViewById(R.id.buttonToA1);
        buttonA2 = findViewById(R.id.buttonToA2);
        final EditText limit = findViewById(R.id.limitNumber);

        final EditText refresh = findViewById(R.id.refreshNumber);



        //Button back to A1
        buttonA1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentA1 = new Intent(getApplicationContext(),A1.class);
                int limitNumber = Integer.valueOf(limit.getText().toString());
                int refreshRate = Integer.valueOf(refresh.getText().toString());
                intentA1.putExtra("link",linkSelected);
                intentA1.putExtra("limit",limitNumber);
                intentA1.putExtra("refresh",refreshRate);
                startActivity(intentA1);

            }
        });


        //Button to A2
        buttonA2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentA2= new Intent(getApplicationContext(),A2.class);
                //intentA2.putExtra("link",linkSelected);
                startActivity(intentA2);

            }
        });





        // Spinner stuff
        spinner = findViewById(R.id.spinner);
        adapter = ArrayAdapter.createFromResource(this,R.array.FeedListNames,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getBaseContext(),adapterView.getItemAtPosition(i)+" selected",Toast.LENGTH_SHORT).show();
                linkSelected = links[i];

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onBackPressed()
    {
        Intent intentToA2= new Intent(getApplicationContext(),A2.class);
        startActivity(intentToA2);
    }
}
