package com.example.sergiogimeno.lab21;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class A2 extends AppCompatActivity {

    String url;
    WebView webView;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a2);
        Intent intent = getIntent();
        url= intent.getStringExtra("link");


        // IF THE URL IS NULL THE ACTIVITY LOADS THE PREVIOUS THING IN IT
        if(url==null)
        {
            loadData();
        }


        button= findViewById(R.id.buttonPreferences);



        // CODE TO CREATE A WEB VIEW INSIDE THE ACTIVITY
        webView= findViewById(R.id.webViewID);
        webView.setWebViewClient(new WebClient());
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.loadUrl(url);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentA3= new Intent(getApplicationContext(),A3.class);
                startActivity(intentA3);

            }
        });
    }

    private class WebClient extends WebViewClient
    {
        public boolean shouldOverrideUrlLoading(WebView view,String url)
        {
            view.loadUrl(url);
            return true;
        }
    }



    //LOAD AN SAVE SHARED PREFERENCES
    protected void onStop()
    {
        super.onStop();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("URL",url);
        editor.apply();
    }

    private void loadData()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        url = prefs.getString("URL",null);
        editor.apply();


    }








    @Override
    public void onBackPressed()
    {
        Intent intentToA1= new Intent(getApplicationContext(),A1.class);
        startActivity(intentToA1);
    }
}
