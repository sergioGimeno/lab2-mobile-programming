package com.example.sergiogimeno.lab21;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class A1 extends AppCompatActivity {
    ListView lv;
    ArrayList<String> titles;
    ArrayList<String> links;
    String address;
    Button button;
    int limit;
    int refresh;

    private Handler handler= new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1);

        lv= findViewById(R.id.LVRss);
        titles = new ArrayList<>();
        links = new ArrayList<>();
        button= findViewById(R.id.buttonPreferences);


        Intent intent = getIntent();
        address = intent.getStringExtra("link");
        limit= intent.getIntExtra("limit",0);
        refresh= intent.getIntExtra("refresh",0);

        //LOAD THE PREVIOUS IF ADDRESS IS NULL
        if(address==null)
        {
            loadData();
        }


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String linkSelected = links.get(i);
                Intent intentA2= new Intent(getApplicationContext(),A2.class);
                intentA2.putExtra("link",linkSelected);
                startActivity(intentA2);

            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentA3= new Intent(getApplicationContext(),A3.class);
                startActivity(intentA3);

            }
        });

        //START ASYNC TASK
        new FetchingRSS().execute();



        //REPEAT THE ASYNC TASK EVERY "refresingFeed"*Seconds
        handler.postDelayed(refreshingFeed,refresh*1000);



    }

    private Runnable refreshingFeed = new Runnable() {
        @Override
        public void run() {
            new FetchingRSS().execute();

            handler.postDelayed(this,refresh*1000);
        }
    };



    // CODE TO GET THE STREAM
    public InputStream getInputStream(URL url)
    {
        try
        {
            return url.openConnection().getInputStream();
        }
        catch (IOException e)
        {
            return null;
        }

    }

    public class FetchingRSS extends AsyncTask<Integer,Integer,Exception>
    {
        ProgressDialog progDialog = new ProgressDialog(A1.this);
        Exception exception= null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDialog.setMessage("Fetching news. Please wait...");
            progDialog.show();
        }

        @Override
        protected void onPostExecute(Exception e) {
            super.onPostExecute(e);
            ArrayAdapter<String> adapter= new ArrayAdapter<>(A1.this,android.R.layout.simple_list_item_1,titles);
            lv.setAdapter(adapter);
            progDialog.dismiss();
        }

        @Override
        protected Exception doInBackground(Integer... integers) {
            try
            {
                URL url = new URL(address);
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(false);
                XmlPullParser pullParser = factory.newPullParser();
                pullParser.setInput(getInputStream(url),"UTF_8");
                boolean insideItem = false;
                int eventType= pullParser.getEventType();
                Log.d("Size is: ",Integer.toString(titles.size()));

                //A WHILE TO CHECK IF IT'S END OF THE DOCUMENT OR IF HAS REACHED TO THE LIMIT
                while((eventType!=XmlPullParser.END_DOCUMENT) && links.size()<limit)
                {

                    //SEARCH FOR THE TAG "START"
                    if(eventType == XmlPullParser.START_TAG )
                    {

                        //INSIDE "START" TAG YOU NEED TO FIND ITEM
                        if(pullParser.getName().equalsIgnoreCase("item"))
                        {
                            insideItem= true;
                        }

                        //YOU FETCH THE TITLE AND LINK OF THE ITEM
                        else if((pullParser.getName().equalsIgnoreCase("title")) && insideItem)
                        {
                            titles.add(pullParser.nextText());
                        }
                        else if((pullParser.getName().equalsIgnoreCase("link")) && insideItem)
                        {
                            links.add(pullParser.nextText());

                        }
                    }
                    //CHECK IF IT HAS REACHED THE END OF THE ITEM
                    else if(eventType == XmlPullParser.END_TAG && pullParser.getName().equalsIgnoreCase("item"))
                    {
                        insideItem=false;
                    }
                    eventType= pullParser.next();
                }
            }

            //EXCEPTIONS REQUIRED
            catch (MalformedURLException e)
            {
                exception=e;
            }
            catch(XmlPullParserException e)
            {
                exception=e;
            }
            catch (IOException e)
            {
                exception= e;
            }
            return exception;
        }
    }


    //LOAD AND SAVE PREFERENCES
    protected void onStop()
    {
        handler.removeCallbacks(refreshingFeed);
        super.onStop();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("URL",address);
        editor.putInt("Limit",limit);
        editor.putInt("refresh",refresh);
        editor.apply();
    }

    private void loadData()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        address = prefs.getString("URL",null);
        limit = prefs.getInt("Limit",0);
        refresh = prefs.getInt("refresh",0);
        editor.apply();


    }



   @Override
    public void onBackPressed()
    {
        moveTaskToBack(true);
    }
}
